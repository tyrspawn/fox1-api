//******************************//
//      FOX ONE RPG API
//      by
//      Briland
//
//
//   Changelog
//   v1 -    2/25/18
//      release
//   v1.1 -    2/26/18
//      Added general SA bonuses to combat SA roller
//      Allowed negative bonuses to all rolls
//      Added wvr ground penalties to NOE and High Alt
//      respelled Maneuver
//      changed reactive attack icon to remove on turn start
//      added maneuver to dogfight rolls
// v1.2 -    2/28/18
//      Added damage roller beta
//      new look for api dice rolls
// v1.3 -    3/02/18
//      Added handling of unknown names to quick roller and combat SA roller
//      Fixed countermeasures working when out
//      Added attack details to output of attack roll
//      Damage macro now shows what roll was made
//      Defense roll negative other bonuses fixed
// v1.4 -    3/04/18
//      Removed range calculation from fire macro
//      fixed quickroll d20 from displaying twice
// v1.5 -    3/07/18
//      Added general sa for combat SA roll
//      Fixed RWR SA bonuses from showing in general sa rolls
//      Changed attack macro so you can only fire 1 bvr weapon per roll
//      Added RWR SA Quickroller
// v2.0 -   10/01/18
//      Updated to support new Character sheets
//      Added altitude function
// v2.1 -   10/06/18
//      Updated attack and defence macro
//      Added lofting, HAS and Energy Mastery functionality
//      Added botch flavor
//      Capitalization
// v2.2 -   10/08/18
//      Added notes section to character sheet
//      Added positional macro to auto check for many more states
//      Automautically initialize attributes to 0 if they dont exist
//      added rewards system
//******************************//

on("ready", function () {
    log("Fox One API commands loading");
});

var attackDB = [];
var attackCounter = 0;
var quickCheck = -1;
var soundLib = {
    gunsVulcan: "BMS_Vulcan.ogg",
    gunsGau8: "A-10 Thunderbolt (Warthog) 30mm GAU cannon 6 by JoltDiana",
    wvrMissle: "small sam",
    bvrMissle: "medium sam",
    bombDrop: "bombdrop",
    wvrCountermeasures: "flare by Gusy",
    bvrCountermeasures: "flare by Gusy",
    afterburnerEngage: "Afterburner",
    bitchingBinbo: "bingo",
    thoughtfullSA: "BMS hmm",
    altitude: "Altitude Gusy by Gusy",
    boomSmall: "BMS_Soft Hit.ogg",
    boomMedium: "BMS_Hard hit.ogg",
    boomBig: "Big Bomb by Gusy",
    botches: [
        "Mission Failed Gusy",
        "Crickets Gusy",
        "Buzzer Gusy",
        "Denied Gusy",
        "Bitch Gusy",
        "CRIT FAIL - Cully by Cully"],
    cardFlip: "Common Gusy by Gusy",
    rareReward: "Rare1 Gusy by Gusy",
    epicReward: "Epic1 Gusy by Gusy",
    legendaryReward: "Legendary1 Gusy by Gusy"
};

var rewardsLib = {
    lightFighter: {
        name: "Light Fighter",
        common: 49,
        rare: 43,
        epic: 44,
        legendary: 2
    },
    heavyFighter: {
        name: "Heavy Fighter",
        common: 48,
        rare: 45,
        epic: 45,
        legendary: 2
    },
    lightAttack: {
        name: "Light Attack",
        common: 53,
        rare: 47,
        epic: 51,
        legendary: 2
    },
    heavyAttack: {
        name: "Heavy Attack",
        common: 52,
        rare: 49,
        epic: 49,
        legendary: 2
    }
};

on("chat:message", function (msg) {

    if (!(msg.type === "api")) {
        return;
    }

    var args = msg.content.split(' '),
        command = args.shift().substring(1),
        sendingPlayer = getObj('player', msg.playerid),
        currentCharacter = getObj('character', sendingPlayer.get('speakingas').split('|')[1]);



    if (command.toLowerCase() === 'burner' && args.length > 0) {

        var currentToken, character_id, attribute;

        if (!(currentToken = getObj('graphic', args[0])))
            return sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});

        if (!(character_id = currentToken.get('represents')))
            return sendChat("FoxOneAPI", sendingPlayer.get('_displayname') + "'s token is not assigned to his character", null, {noarchive:true});

        if (!(attribute = getAttrObj(character_id, "AB"))) 
            return sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You have not set your afterburner in your character sheet", null, {noarchive:true});

        var numAB = attribute.get('current');
        if (numAB < 1) {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have any Afterburner fuel remaining", null, {noarchive:true});
            return;
        }

        currentToken.set("status_yellow", true); 
        if (typeof args[1] == 'undefined' || args[1] != 'silent') {
            sendChat("character|"+character_id, "/em Engages burner");
        }
        setAttribute(character_id,"AB", numAB-1);
        playTrack(getTrackbyName(soundLib.afterburnerEngage));
        if (numAB == 1)
            playTrack(getTrackbyName(soundLib.bitchingBinbo));
    }

    if (command.toLowerCase() === 'altitude' && args.length > 0) {

        var currentToken = getObj('graphic', args[0]);
        if (typeof currentToken == "undefined") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});
            return;
        }

        var character_id = currentToken.get('represents');
        if (character_id == "") {
            sendChat("FoxOneAPI", sendingPlayer.get('_displayname') + "'s token is not assigned to his character", null, {noarchive:true});
            return;         
        }


        if (currentToken.get("status_angel-outfit")) {
            currentToken.set("status_angel-outfit", false);
            sendChat("character|"+character_id, "/em Drops to low altitude");
        }
        else {
            currentToken.set("status_angel-outfit", true);
            sendChat("character|"+character_id, "/em Climbs to high altitude");
        }
        playTrack(getTrackbyName(soundLib.altitude));   
    }

    if (command.toLowerCase() === 'radar' && args.length > 0) {

        var currentToken = getObj('graphic', args[0]);
        if (typeof currentToken == "undefined") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});
            return;
        }

        var character_id = currentToken.get('represents');
        if (character_id == "") {
            sendChat("FoxOneAPI", sendingPlayer.get('_displayname') + "'s token is not assigned to his character", null, {noarchive:true});
            return;         
        }

        var attribute = findObjs({
            _type: "attribute",
            _characterid: character_id,
            _name: "Radar"
        })[0];
        if (typeof attribute == "undefined") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You have not set your radar in your character sheet", null, {noarchive:true});
            return;         
        }

        var radarRange = attribute.get('current');
        if (radarRange < 1) {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a radar", null, {noarchive:true});
            return;
        }

        if(currentToken.get("status_green")) {
            if (typeof args[1] == 'undefined' || args[1] != 'silent') {
                sendChat("character|"+character_id, "/em Disables Radar");
            }
            currentToken.set("status_green", false);    
            currentToken.set('light_radius', 0);
            currentToken.set('light_dimradius', '');


        } else {
            if (typeof args[1] == 'undefined' || args[1] != 'silent') {
                sendChat("character|"+character_id, "/em Enables Radar");
            }
            currentToken.set("status_green", true); 
            currentToken.set('light_radius', radarRange);
            currentToken.set('light_dimradius', radarRange);
        }
    }

    if (command.toLowerCase() === 'combatsa' && args.length > 0) {

        var currentToken = getObj('graphic', args[0]);
        if (typeof currentToken == "undefined") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});
            return;
        }
        var token_id = currentToken.get('_id');
        var character_id = currentToken.get('represents');
        if (character_id == "") {
            sendChat("FoxOneAPI", sendingPlayer.get('_displayname') + "'s token is not assigned to his character", null, {noarchive:true});
            return;         
        }

        var turnorder;      
        var characterobj = getObj('character', character_id);

        var attrSA = parseInt(getAttrByName(character_id, "SA"));

        var bonuses = getGeneralBonuses("SA",character_id);
        for (var i = 0; i < bonuses.length; i++){
            rollArray.push({type:bonuses[i].name,value:bonuses[i].value});
        }

        if(typeof attrSA == "undefined" || attrSA === "") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You have not set your SA in your character sheet", null, {noarchive:true});
            return;         
        }

        if(Campaign().get("turnorder") == "") 
            turnorder = []; //NOTE: We check to make sure that the turnorder isn't just an empty string first. If it is treat it like an empty array.
        else 
            turnorder = JSON.parse(Campaign().get("turnorder"));

        for (var i = 0; i<turnorder.length;i++) {
            if (turnorder[i].id == token_id) {
                sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You are already in the turn order.", null, {noarchive:true});
                return;                     
            }
        }

        var rollArray = [];
        var rollresult = roll(20);
        var rolltotal = 0;

        rollArray.push({type:'roll',value:rollresult,die:20});
        rolltotal += rollresult;

        rollArray.push({type:'SA',value:attrSA});
        rolltotal += attrSA;

        var bonuses = getGeneralBonuses('sa',character_id);
        for (var i = 0; i < bonuses.length; i++){
            rollArray.push({type:bonuses[i].name,value:bonuses[i].value});
            rolltotal += parseInt(bonuses[i].value);
        }

        //Add a new custom entry to the end of the turn order.
        turnorder.push({
            id: token_id,
            pr: rolltotal,
        });
        Campaign().set("turnorder", JSON.stringify(turnorder));

        //handle empty name
        var tokenName = currentToken.get('name');
        if (currentToken.get('layer') != 'gmlayer')
            if (tokenName == '' || (!currentToken.get('showplayers_name')) ) {
                tokenName = 'Unknown '+ getTokenVehType(currentToken) +' contact';
                sendChat(tokenName, "/em Prepares for combat <br>" + explodePrintRoll(rollArray, sendingPlayer.get('color')));
            }
            else
                sendChat("character|"+characterobj.get('id'), "/em Prepares for combat <br>" + explodePrintRoll(rollArray, sendingPlayer.get('color')));
    } 

    if (command.toLowerCase() === 'fire' && args.length > 1) {

        var aggressorToken = getObj('graphic', args[0]);
        if (typeof aggressorToken == "undefined") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});
            return;
        }

        var character_id = aggressorToken.get('represents');
        if (character_id == "") {
            sendChat("FoxOneAPI", aggressorToken.get('name') + "'s token is not assigned to a character", null, {noarchive:true});
            return;         
        }

        var targetToken = getObj('graphic', args[1]);
        if (typeof targetToken == "undefined") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " Invalid or no target token selected", null, {noarchive:true});
            return;
        }

        var bvrRangeModifier = getSprintBonus(aggressorToken);
        var targetType = getTokenVehType(targetToken);
        var weapons = getWeaponLoadout(character_id);
        weapons.sort(function(a, b){return a.pylon - b.pylon});

        var canFire = false;
        var targetDistance = tokenDistance(aggressorToken, targetToken);
        var aggressorAltitude = getAltState(aggressorToken);

        var outputStr = "";   
        var buttonColor = '#CE0F69';

        for (var i = 0; i < weapons.length; i++){

            var weaponRange = parseInt(weapons[i].range);

            if (weapons[i].type=='bvr') {
                weaponRange += bvrRangeModifier;
            }
            if (weapons[i].quantity > 0 
                && (weapons[i].target == targetType || weapons[i].type == "gun")
        //&& (weaponRange >= targetDistance) //check distance
        && !(weapons[i].type == "gun" && aggressorAltitude == 'high' && targetType == 'ground')
        ) {

                canFire = true;

            var askHowMany = "1";
            if ((weapons[i].type != "bvr" && targetType == 'ground'))
                askHowMany = "?{How many to fire|1}"

            if (weaponRange < targetDistance)
                buttonColor = '#a8a8a8';
            else
                buttonColor = '#CE0F69';

            outputStr = outputStr + "<a style='background-color:"+buttonColor+";' href='!attackroll "+aggressorToken.get('_id')+" "+weapons[i].id+" "+targetToken.get('_id')+ " ?{Other Bonuses?|0} "+askHowMany+" "+weaponRange+" "+weaponSpeed+"'>" + weapons[i].name + "</a> <span style='float:right;'> Quantity: " + weapons[i].quantity + "</span><br>";
        } else {
            outputStr = outputStr + "<a style='background-color:#bfbfbf;' href='!'>" + weapons[i].name + "</a> <span style='float:right;'> Quantity: " + weapons[i].quantity + "</span><br>";
        }
    }
    if (!canFire) {
        outputStr = " You do not have any weapons systems that can engage the selected target";
    }

    sendChat("FoxOneAPI", "/w "+ sendingPlayer.get('_displayname') +" <div>" + outputStr + "</div>", null, {noarchive:true});
    }

    if (command.toLowerCase() === 'attackroll' && args.length == 7) {

        var rollArray = [];
        rollArray.push({type:'roll',value:roll(20),die:20});

        var aggressorToken = getObj('graphic', args[0]);
        var weapon = findWeaponbyCharacter(args[1], aggressorToken.get('represents'));
        var targetToken = getObj('graphic', args[2]);
        var otherBonus = parseInt(args[3]);
        var numToFire = parseInt(args[4]);
        var weaponRange = parseInt(args[5]);
        var weaponSpeed = parseInt(args[6]);

    // Get characterID for output
    if (typeof aggressorToken == "undefined") {
        sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});
        return;
    }
    var character_id = aggressorToken.get('represents');
    if (character_id == "") {
        sendChat("FoxOneAPI", sendingPlayer.get('_displayname') + "'s token is not assigned to his character", null, {noarchive:true});
        return;
    }

    var targetVehType;
    var targetCharacter = targetToken.get('represents');
    if (targetCharacter == "") {
        log("FoxOneAPI: Target token for attack macro has no character sheet, assuming a ground target");
        targetVehType = 'ground';
    } else {
        targetVehType = getAttrByName(targetCharacter, 'vehType');
    }

    if (numToFire > weapon.quantity || weapon.quantity < 1){
        sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You have tried to fire weapons you do not have avaliable", null, {noarchive:true});
        log("FoxOneAPI ERROR: " +sendingPlayer.get('_displayname') + " tried to fire weapons they did not have avaliable");
        return;
    }

    //build roll array
    rollArray.push({type:'Weapon',value:weapon.attack});
    rollArray = rollArray.concat(getAtkBonuses(aggressorToken, targetVehType, weapon.type, getAltState(aggressorToken), weapon.target));

    if (otherBonus != 0) rollArray.push({type:'Other Bonus',value:otherBonus});
    if (getAltState(aggressorToken) == 'high' && targetVehType == 'ground' && weapon.type != 'bvr') rollArray.push({type:'High Altitude',value:-10});

    //output num to fire
    var numRepresentation = 'a';
    if (numToFire > 1)
        numRepresentation = numToFire;

    //handle empty name
    var targetName =targetToken.get('name');
    if (targetName == '' || !targetToken.get('showplayers_name')) {
        targetName = 'Unknown '+ targetVehType +' target';
    }

    var actionVerb = 'Fires';
    //Lofting
    if (hasLofting(aggressorToken) && weapon.type == 'wvr' && targetVehType == 'ground' && getAltState(aggressorToken) == 'low') {
        aggressorToken.set("status_chemical-bolt", true);
        actionVerb = 'Lofts';
    }

    //output to chat
    var output = explodePrintRoll(rollArray, sendingPlayer.get('color'));
    sendChat('character|'+character_id, "/me "+actionVerb+" "+numRepresentation+" "+weapon.name+" at "+targetName+"! <br>" + output + "<br><br>Target Range:"+tokenDistance(aggressorToken, targetToken));

    subtractWeaponQuantity(weapon.id, character_id, numToFire);

    //Play Sounds
    if (weapon.type == 'gun') 
        playTrack(getTrackbyName(soundLib.gunsVulcan));
    else if (weapon.type == 'bvr') 
        playTrack(getTrackbyName(soundLib.bvrMissle));
    else if (weapon.type == 'wvr' && targetVehType == 'ground')
        playTrack(getTrackbyName(soundLib.bombDrop));
    else if (weapon.type == 'wvr')
        playTrack(getTrackbyName(soundLib.wvrMissle));

    //Crit Sounds
    if (rollArray[0].value ==1){
        var randomBotch = soundLib.botches[Math.floor(Math.random() * soundLib.botches.length)];
        playTrack(getTrackbyName(randomBotch));
    }
    }

    if (command.toLowerCase() === 'defenceroll' && args.length == 4) {

        var type = args[1];
        var use_countermeasures = parseInt(args[2]);
        var otherBonus = parseInt(args[3]);

        var rollArray = [];
        var counterMeasuresStrength = 0;

    // Get characterID for output
    var defenderToken = getObj('graphic', args[0]);
    if (typeof defenderToken == "undefined") {
        sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});
        return;
    }
    var character_id = defenderToken.get('represents');
    if (character_id == "") {
        sendChat("FoxOneAPI", sendingPlayer.get('_displayname') + "'s token is not assigned to his character", null, {noarchive:true});
        return;         
    }

    rollArray.push({type:'roll',value:roll(20),die:20});

    if (type == 'WVR') {

        var attribute = getAttrByName(character_id, 'Maneuver');
        rollArray.push({type:'Maneuver',value:attribute});

        if (isBurning(defenderToken)) {
            rollArray.push({type:'Afterburner',value:2});
            if (hasEnergyMastery(defenderToken)) {
             rollArray.push({type:'Energy Mastery',value:1});
         }
     }

     if (isLofting(defenderToken)) {
        rollArray.push({type:'Lofting',value:5});
    }

    if (use_countermeasures > 0){
        counterMeasuresStrength = subtractCounterMeasureQuantity('WVR', character_id);
        if (counterMeasuresStrength == 0)
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You are out of flares.", null, {noarchive:true});
    }

    if (counterMeasuresStrength != 0) {
        rollArray.push({type:'Flares',value:counterMeasuresStrength});
    }

    if (getAltState(defenderToken) == 'high') {
        rollArray.push({type:'High Altitude Penalty',value:-5});
    }

    }

    if (type == 'BVR') {

        var attribute = getAttrByName(character_id, 'Tactics');
        rollArray.push({type:'Tactics',value:attribute});

        if (isBurning(defenderToken)) {
            rollArray.push({type:'Afterburner',value:2});
            if (hasEnergyMastery(defenderToken)) {
                rollArray.push({type:'Energy Mastery',value:1});
            }
        }

        if (use_countermeasures > 0){
            counterMeasuresStrength = subtractCounterMeasureQuantity('BVR', character_id);
            if (counterMeasuresStrength == 0)
                sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You are out of chaff.", null, {noarchive:true});
        }

        if (counterMeasuresStrength != 0) {
            rollArray.push({type:'Chaff',value:counterMeasuresStrength});
        }
    }

    if (otherBonus != 0) {
        rollArray.push({type:'Other',value:otherBonus});
    }

    var output = explodePrintRoll(rollArray, sendingPlayer.get('color'));
    sendChat('character|'+character_id, "/me Defends a "+type+ " attack! <br>" + output);

    //Play Sounds
    if (counterMeasuresStrength > 0) {
        if (type == 'BVR') {
            playTrack(getTrackbyName(soundLib.bvrCountermeasures));
        }
        if (type == 'WVR') {
            playTrack(getTrackbyName(soundLib.wvrCountermeasures));
        }
    }
    }

    /*
    Macros for quickroller
    SA
    !roll @{selected|token_id} 0 SA 20
    Positional
    !roll @{selected|token_id} 0 Positional 20 ?{Maneuver Bonus From Afterburner|0} ?{Maneuver Bonus From Formations|0}
    D20
    !roll @{selected|token_id} 0 d20 20
    */
    // Arguments @{selected|token_id} {gmroll} ?{Type of Roll} {dicesize} {rollspecific 1} {rollspecific 2}
    if (command.toLowerCase() === 'roll' && args.length > 1) {

        var rollArray = [];
        var dieSize = 20;
        var gmroll = parseInt(args[1]);

        if (args.length > 3){
            dieSize = parseInt(args[3]);
            if (dieSize < 1) return;
        }

        var rollType = args[2];

        // Get characterID for output
        var currentToken = getObj('graphic', args[0]);
        if (typeof currentToken == "undefined") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});
            return;
        }
        var character_id = currentToken.get('represents');
        if (character_id == "") {
            sendChat("FoxOneAPI", sendingPlayer.get('_displayname') + "'s token is not assigned to his character", null, {noarchive:true});
            return;         
        }

        //Roll the dice
        rollArray.push({type:'roll',value:roll(dieSize),die:dieSize});


        // SA Roll
        if (rollType == 'SA' || rollType == 'sa-rwr') {
            rollArray.push({type:'SA',value:getAttrByName(character_id, 'SA')});
        }

        if (rollType == 'Positional'){

            rollArray.push({type:'Maneuver',value:getAttrByName(character_id, 'Maneuver')});
            rollArray.push({type:'Positional',value:getAttrByName(character_id, 'Positional')});

            //Check for AB Bonus
            if (isBurning(currentToken)) {
                rollArray.push({type:'Afterburner',value:2});
                if (hasEnergyMastery(currentToken)) {
                    rollArray.push({type:'Energy Mastery',value:1});
                }
            }

            if (isLofting(currentToken)) {
                rollArray.push({type:'Lofting',value:5});
            }

            if (args.length > 4) {
                if (parseInt(args[4]) != 0)
                 rollArray.push({type:'Formations',value:parseInt(args[5])});
            }

            if (getAltState(currentToken) == 'high') {
                rollArray.push({type:'High Altitude Penalty',value:-5});
            }
        }

        //Play Sounds
        if (rollType == 'SA') {
            //if (gmroll < 1)
            playTrack(getTrackbyName(soundLib.thoughtfullSA));
        }

        //Check to see if this is a secret GM roll
        if (gmroll > 0) {
            sendChat("FoxOneAPI", "/w GM "+currentToken.get('name')+" rolls a d"+dieSize+" for "+rollType+"<br>"+ explodePrintRoll(rollArray ,sendingPlayer.get('color')));
        } else {
            if (rollType == 'd20')
                sendChat("character|"+character_id, "/me rolls a d"+dieSize+"<br>"+ explodePrintRoll(rollArray ,sendingPlayer.get('color')));
            else {
                //handle empty name
                var tokenName = currentToken.get('name');
                if (tokenName == '' || (!currentToken.get('showplayers_name')) ) {
                    tokenName = 'Unknown '+ getTokenVehType(currentToken) +' contact';
                    sendChat(tokenName, "/me rolls a d"+dieSize+" for "+rollType+"<br>"+ explodePrintRoll(rollArray ,sendingPlayer.get('color')));
                }
                else
                    sendChat("character|"+character_id, "/me rolls a d"+dieSize+" for "+rollType+"<br>"+ explodePrintRoll(rollArray ,sendingPlayer.get('color')));
            }
        }
    }

    if (command.toLowerCase() === 'playtrack' && args.length == 1) {

        var sound = getTrackbyName(args[0]);
        playTrack(sound, sendingPlayer);
    }

    if (command.toLowerCase() === 'rolldamage' && args.length == 4) {

        // Get characterID for output
        var currentToken = getObj('graphic', args[0]);
        if (typeof currentToken == "undefined") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});
            return;
        }
        var character_id = currentToken.get('represents');
        if (character_id == "") {
            sendChat("FoxOneAPI", sendingPlayer.get('_displayname') + "'s token is not assigned to his character", null, {noarchive:true});
            return;         
        }

        var diceRoll = args[1];
        var diceRollArray = diceRoll.split('d');
        var numdie = parseInt(diceRollArray[0]);
        var dieSize = parseInt(diceRollArray[1]);

        var armor = parseInt(args[3]);
        var armorPierce = parseInt(args[2]);;

        var damageModifier = armorPierce - armor;
        if (damageModifier > 0) damageModifier = 0;

        var rollArray = [];
        var armorOutput = '(0 Armor)';
        var apOutput = '(0 AP)';

        if (armor != 0) armorOutput = '('+ armor + " Armor)";
        if (armorPierce != 0) apOutput = '('+ armorPierce + " AP)";

        for(var i = 0; i<numdie; i++){
            rollArray.push({type:'roll',value:roll(dieSize),die:dieSize,modifier:damageModifier});
        }

        //Play Sounds
        var totalDamage = sumRoll(rollArray);
        if (totalDamage <= 4) 
            playTrack(getTrackbyName(soundLib.boomSmall));
        else if (totalDamage <= 20) 
            playTrack(getTrackbyName(soundLib.boomMedium));
        else
            playTrack(getTrackbyName(soundLib.boomBig));

        sendChat("character|"+character_id, "/me rolls "+diceRoll+" for damage "+apOutput+" against " + armorOutput +"!<br>"+ explodePrintRoll(rollArray ,sendingPlayer.get('color')));
    }

    if (command.toLowerCase() === 'rewards' && args.length == 3) {

        // Get characterID for output
        var currentToken = getObj('graphic', args[0]);
        if (typeof currentToken == "undefined") {
            sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " You do not have a valid token selected.", null, {noarchive:true});
            return;
        }
        var character_id = currentToken.get('represents');
        if (character_id == "") {
            sendChat("FoxOneAPI", sendingPlayer.get('_displayname') + "'s token is not assigned to his character", null, {noarchive:true});
            return;         
        }

        var classInt = parseInt(args[1]); // 1- lightFighter, 2- heavyFighter, 3- lightAttack, 4- heavyAttack
        var numRewards = parseInt(args[2]);

        var classLibObj = undefined;
        switch (classInt) {
            case 1:
            classLibObj = rewardsLib.lightFighter;
            break;
            case 2:
            classLibObj = rewardsLib.heavyFighter;
            break;
            case 3:
            classLibObj = rewardsLib.lightAttack;
            break;
            case 4:
            classLibObj = rewardsLib.heavyAttack;
            break;
            default:
            log("FoxOne API ERROR: Undefined class in rewards function");
            return;
        }

        // loop through rewards
        for (var i = 0; i < numRewards; i++) {

            var reward = {
                num: i+1,
                total: numRewards,
                class: classLibObj,
            }

            setTimeout(sayRewards(character_id,reward), 3000*i);
         //sendChat("character|"+character_id, "/em Draws " + classLibObj.name +" " + rarityName +" " +cardNumber);
        }
    }

    // Debug Functions
    if (command.toLowerCase() === 'test' && args.length > 0) {

        if (args[0] == 'radaron') return quickCheck = parseInt(args[1]);
        if (args[0] == 'radaroff') return quickCheck = -1;

        //var currentToken = findTokenFromCharacter(currentCharacter.get('_id'));
        //var distance = tokenDistance(currentToken, otherToken);

        var splitTest 
        splitTest = "3d6".split('d');
        //log(splitTest);
        splitTest = "4d6".split('d');
        //log(parseInt('0'+splitTest[0]));

        //getAttrObj('-L5H3S-kpenSIiJAxSdU', 'WvrCMQuantity');
    }

    if (command.toLowerCase() === 'logattr') {

        var selectedToken;

        if(msg.selected) {
            selectedToken = getObj("graphic", msg.selected[0]._id);
        } else {
            sendChat("ERROR"," No token selected.");
            return;
        }

        var attrs = findObjs({
            _type: "attribute",
            _characterid: selectedToken.get('represents'),
        });
        log(attrs);
    }

});

on("change:campaign:turnorder", function(obj) {

    removeTurnTokens();
});

function sayRewards(character_id, reward){
    return function() {  

        var rarityNumber = roll(1000);
        var cardNumber = 0;
        var rarityName = '';
        var sound = false;

        //Legendary
        if (rarityNumber > 996) {
            cardNumber = roll(reward.class.legendary);
            rarityName = 'Legendary';
            sound = soundLib.legendaryReward;
        }
        //Epic
        else if (rarityNumber > 940) {
            cardNumber = roll(reward.class.epic);
            rarityName = 'Epic';
            sound = soundLib.epicReward;
        }
        //Rare
        else if (rarityNumber > 700) {
            cardNumber = roll(reward.class.rare);
            rarityName = 'Rare';
            sound = soundLib.rareReward;
        }
        //Common
        else {
            cardNumber = roll(reward.class.common);
            rarityName = 'Common';
            sound = false;
        }

        sendChat("character|"+character_id, "/em Draws "+reward.num+"/"+reward.total +"<br>" + rewardCardBox(reward.class.name, rarityName, cardNumber));

        playTrack(getTrackbyName(soundLib.cardFlip));
        if (sound)
            playTrack(getTrackbyName(sound));

    }
}

function rewardCardBox(rewardClass, rewardRarity, rewardNumber) {

    var borderColor = '#7c7e8c';

    switch (rewardRarity) {
        case "Legendary":
        borderColor = '#e0a616';
        break;
        case "Epic":
        borderColor = '#9915e0';
        break;
        case "Rare":
        borderColor = '#1529e0';
        break;
        default:
        borderColor = '#7c7e8c';
        break;
    }

    var backgroundColor = shadeColor(borderColor, 0.9);
    var textColor = borderColor;
    var output ="";
    output += "<div style='background-color:"+backgroundColor+"; color:"+textColor+"; font-weight:bold; margin-top: 5px; padding: 2px 5px; border-radius:6px; border: 2px solid "+borderColor+";'>";
    output += rewardClass + "<br><br>";
    output += "<span style='font-size:40px'>"+ rewardNumber + "</span><br>";
    output += rewardRarity + "</div>";
    return output;
}

// Deactivates all tokens for the person ontop of the turn order
function removeTurnTokens(){
    var turn_order = JSON.parse(Campaign().get("turnorder"));
// turn off AB for top person in turn order
if (turn_order != 'undefined' && turn_order != '') {
    currentToken = getObj('graphic', turn_order[0].id);
    if (currentToken != 'undefined') {
        currentToken.set("status_yellow", false); 
        currentToken.set("status_all-for-one", false); 
        currentToken.set("status_chemical-bolt", false); 
    }
}
}

function getGeneralBonuses(attrName, character_id){

    var bonuses = [];

    var attrs = findObjs({
        _type: "attribute",
        _characterid: character_id,
    });
    for (var i = 0; i < attrs.length; i++) {

// HTML needs to be updated for this to work properly
if (attrs[i].get('name').endsWith('generalBonusAttribute') && attrs[i].get('current').endsWith(attrName)) {

    var bonusID = attrs[i].get('name').split('_')[2];
    bonuses.push(getGeneralBonus(bonusID,attrs));
}
}

return bonuses;
}

function getGeneralBonus(bonusId, attributes){

    var bonus= {
        id: bonusId,
        attribute: '',
        name: '',
        value: 0
    };

    for (var i = 0; i < attributes.length; i++) {
        var atributeName = attributes[i].get('name');
        if (atributeName.includes(bonusId)) {
            if (atributeName.endsWith('generalBonusAttribute')){
                bonus.attribute = attributes[i].get('current');
            }
            if (atributeName.endsWith('generalBonusName')){
                bonus.name = attributes[i].get('current');
            }
            if (atributeName.endsWith('generalBonusValue')){
                bonus.value = parseInt(attributes[i].get('current'));
            }
        }
    }

    return bonus;
}

function hasHighAltitudeSprint(token){

    return hasSkill(token,'HighAltitudeSprint');
}

function hasEnergyMastery(token){

    return hasSkill(token,'EnergyMastery');
}

function hasLofting(token){

    return hasSkill(token,'Lofting');   
}

function hasSkill(token, skill) {
    var characterid = token.get('represents');
    if (characterid == '') {
        log('FoxOneAPI ERROR: getSkill null characterid');
        return 0;
    }

    var attr = getAttrByName(characterid, skill);

    if (attr === 'true') 
        return true;

    return false;
}

function isBurning(token) {

    if (token.get("status_yellow"))
        return true;
    return false;
}

function isLofting(token) {

    if (token.get("status_chemical-bolt"))
        return true;
    return false;
}

function getSprintBonus(token){

    var characterid = token.get('represents');
    if (characterid == '') {
        log('FoxOneAPI ERROR: getSprintBonus null characterid');
        return 0;
    }
    var sprintLevel = 0;
//if target is in Hight altitude sprint extend range with sprint level
if (token.get("status_angel-outfit") && token.get("status_yellow") && hasHighAltitudeSprint(token)){
    sprintLevel = 1;
}
return sprintLevel;
}

function getTokenVehType(token){
    var targetType;
    if (token.get('represents') != "") {
        var targetType = getAttrByName(token.get('represents'), 'vehType');
        if (typeof targetType == "undefined" || targetType == "") targetType = 'ground';
    } else {
        targetType = 'ground';
    }
    return targetType;
}

function getAltState(token){
    if (token.get('status_angel-outfit')) {
        return 'high';
    }
    return 'low';
}

function getAtkBonuses(token, targetTokenType='ground', weaponType, aircraftStatus, weaponTarget) {
    var rollArray = [];
    var bonuses = [];

    var character_id = token.get('represents');
    if (character_id == '') return [];

    //Get bonuses from character sheet
    var wvrAttack = getAttrInt(character_id, "WvrAttack");
    var bvrAttack = getAttrInt(character_id, "BvrAttack");
    var gunAttack = getAttrInt(character_id, "GunAttack");
    var groundAttack = getAttrInt(character_id, "GroundAttack");
    var hagAttack = getAttrInt(character_id, "HagAttack");

    //select bonuses that apply here
    if (weaponType == 'wvr' || weaponType == 'gun') {
        rollArray.push({type:"WVR Attack",value:wvrAttack});
    }
    if (weaponType == 'bvr') {
        rollArray.push({type:"BVR Attack",value:bvrAttack});
    }
    if (weaponType == 'gun') {
        rollArray.push({type:"Gun Attack",value:gunAttack});
    }
    if (targetTokenType == 'ground') {
        rollArray.push({type:"Ground Attack",value:groundAttack});
    }
    if (aircraftStatus == 'high' && targetTokenType == 'ground' && weaponType != 'bvr') {
        rollArray.push({type:"High Altitude Bonus",value:hagAttack});
    }

    return rollArray;
}

function getTrackbyName(name){

    var songs = findObjs({
        _type: "jukeboxtrack",
    });

    for (var i = 0; i<songs.length; i++){
        if (songs[i].get('title').toLowerCase().includes(name.toLowerCase()))
            return songs[i];
    }
    log('FoxOneAPI ERROR: No track found with name: '+name);
    return;
}

function playTrack(trackObj, sendingPlayer='GM'){

    if (typeof trackObj == 'undefined') {
        //sendChat("FoxOneAPI", "/w " + sendingPlayer.get('_displayname') + " No track found with the title given.", null, {noarchive:true});
        log('FoxOneAPI ERROR: undefined track in playTrack');
        return false;         
    }

    trackObj.set('playing', true);
    trackObj.set('softstop', false);
    trackObj.set('loop', false);
    return true;
}

function sumRoll(roll) {

    var sum = 0;
    for(var i = 0; i<roll.length; i++){

        // Only add dice to result if total value after modifier is positive
        if (typeof roll[i].modifier != 'undefined') {
            var tempSum = parseInt(roll[i].value) + roll[i].modifier;
            if (tempSum > 0) sum += tempSum;
        }
        // If there's not a modifier just add the value
        else {
            sum += parseInt(roll[i].value);
        }
    }
    return sum
}

function explodePrintRoll(roll, playercolor) {
    var output = "<div style='padding:5px'>"
    var sum = 0;
    for(var i = 0; i<roll.length; i++){

        // Only add dice to result if total value after modifier is positive
        if (typeof roll[i].modifier != 'undefined') {
            var tempSum = parseInt(roll[i].value) + roll[i].modifier;
            if (tempSum > 0) sum += tempSum;
        }
        // If there's not a modifier just add the value
        else {
            sum += parseInt(roll[i].value);
        }

        output += "<div style='padding:2px; margin:3px;'>";

        // If the box is the second box or greater add a + before the contents
        if (i > 0) output += " + ";

        // If the boxtype is a roll show the dice colored block 
        if (roll[i].type == 'roll') {

             // If there is a modifier assigned to this roll show it
            if (typeof roll[i].modifier != 'undefined' && roll[i].modifier != 0) {
                output +=  diceColoredBox(roll[i].value, roll[i].die, playercolor) + " " + roll[i].modifier +" <br>";
            }
            else {
                output += diceColoredBox(roll[i].value, roll[i].die, playercolor);
            }

        // else show the default colored constant block
        } else {
            output += coloredBox(roll[i].type + " (" +roll[i].value+")");
        }

        output += "</div>";
    }
    output += "</div>= " + coloredBox(sum, 'LimeGreen');
    return output;
}

function diceColoredBox (roll, die, playercolor='', modifier=0){
    var color = "blue";
    var outText = "("+roll+")";
    if (roll == 1) color = "red";
    if (roll == die) color = "green";
    return coloredBox(outText, shadeColor(playercolor, 0.9), color, playercolor);
}

function coloredBox (text, backgroundcolor = '#cccccc', textcolor='black', playercolor='') {
    var code;
    var bordercolor = 'black';
    if (! playercolor == '') {
        bordercolor = playercolor;
    //backgroundcolor = 'white';
    //textcolor = 'black';
    }
    switch(textcolor) {
        case 'red':
        textcolor = "red";
        break;
        case 'green':
        textcolor = "green";
        break;      
    }

    return "<span style='background-color:"+backgroundcolor+"; color:"+textcolor+"; font-weight:bold; margin-top: 5px; padding: 2px 5px; border-radius:6px; border: 2px solid "+bordercolor+";'>" + text + "</span>";

    /*
    <svg height="20" width="20">
    <polygon points="5,2 14,2 19,10 14,18 5,18 1,10" style="fill:White;stroke:black;stroke-width:1" />
    </svg>*/
}

function roll(dice) {

    if (typeof dice != 'number') {
        log('FoxOneAPI ERROR: When rolling dice type must be of type number');
        return -1;
    }
    if (dice < 1) {
        log('FoxOneAPI ERROR: When rolling dice, dice size must be greater than 0');
        return -1;
    }

    var ran = settle(Math.random() * dice);
    return ran;
}

function shadeColor(color, percent) {   
    var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

function setAttribute(character_id, attr_name, newVal) {
    var attribute = findObjs({
        _type: "attribute",
        _characterid: character_id,
        _name: attr_name
    })[0];

    if (attribute === undefined) {
        createObj("attribute", {
            name: attr_name,
            current: newVal,
            characterid: character_id
        });
    } else {
        attribute.set("current", newVal.toString());
    }
}

function subtractWeaponQuantity(weaponId, character_id, numToFire='1'){

    var attrs = findObjs({
        _type: "attribute",
        _characterid: character_id,
    });

    for (var i = 0; i < attrs.length; i++) {
        if (attrs[i].get('name').endsWith('weaponQuantity') && attrs[i].get('name').includes(weaponId)) {
            attrs[i].set('current', attrs[i].get('current') - numToFire );
            return true;
        }
    }
    log("FoxOneAPI ERROR: weapon quantity not sucesfully subtracted, weapon: " + weaponId);
    return false;
}

function subtractCounterMeasureQuantity(cm_type, character_id, numToFire='1'){

    var quantity_attr;
    var strength_attr;
    if (cm_type == 'WVR'){
        quantity_attr = getAttrObj(character_id, 'WvrCMQuantity');
        strength_attr = getAttrObj(character_id, 'WvrCMStrength');
    }
    else if (cm_type == 'BVR'){
        quantity_attr = getAttrObj(character_id, 'BvrCMQuantity');
        strength_attr = getAttrObj(character_id, 'BvrCMStrength');
    }
    else return 0;

    if (quantity_attr === undefined || strength_attr === undefined) {
        log("FoxOneAPI ERROR: countermeasure quantity not sucesfully subtracted, character: " + character_id);
        return 0;
    }

    if (quantity_attr.get('current') > 0) {
        quantity_attr.set('current', parseInt(quantity_attr.get('current')) - numToFire);
        return parseInt(strength_attr.get('current'));
    }

    return 0;
}

//finds the token on the current page that represents the character given
function findTokenFromCharacter(character_id){
    var curPageID = Campaign().get("playerpageid");
    var token = findObjs({
        _type: "graphic",
        represents: character_id,
        _pageid: curPageID
    });
    return token[0];
}

function settle(number){
    if (quickCheck>0) {number = Math.floor(Math.abs(quickCheck - 0.1)); quickCheck = -1;}
    return Math.floor(number + 1);
}

function getAttrInt(character_id, attr_name, select='current'){
    var obj = getAttrObj(character_id, attr_name);

    if (obj === undefined || obj == false)
        return 0;

    return parseInt(obj.get(select));
}

function getAttrObj(character_id, attr_name){
    var attribute = findObjs({
        _type: "attribute",
        _characterid: character_id,
        name: attr_name
    })[0];
    if (attribute === undefined){
        log("FoxOne API WARNING: No attribute " + attr_name + " for character" + character_id +"found. Initializing to 0");
        setAttribute(character_id, attr_name, 0);
        return getAttrObj(character_id, attr_name);
    }
    return attribute;
}

// takes in a character id and returns an array of weapon objects
function getWeaponLoadout(character_id) {

    var weapons = [];

    var attrs = findObjs({
        _type: "attribute",
        _characterid: character_id,
    });
    for (var i = 0; i < attrs.length; i++) {
        if (attrs[i].get('name').endsWith('weaponName')) {
            var pylonid = attrs[i].get('name').split('_')[2];
            weapons.push(findWeapon(pylonid,attrs));
        }
    }

    return weapons;
}

function findWeaponbyCharacter(weapon_id, character_id){

    var attrs = findObjs({
        _type: "attribute",
        _characterid: character_id,
    });
    return findWeapon(weapon_id, attrs);
}

// takes in a character id and list of character attributes and returns a weapon object
// {id, name, pylon, type, target, attack, damage, quantity, range, speed, ap}
function findWeapon(weapon_id, attributes) {

    var found= false;
    var id = weapon_id;
    var name;
    var pylon;
    var type;
    var target;
    var attack;
    var damage;
    var ammo;
    var range = 1;
    var speed = 6;
    var ap = 0;

    for (var i = 0; i < attributes.length; i++) {

        var atributeName = attributes[i].get('name');
        if (atributeName.includes(weapon_id)) {
            found= true;
            if (atributeName.endsWith('weaponName')){
                name = attributes[i].get('current');
            }
            if (atributeName.endsWith('pylonName')){
                pylon = attributes[i].get('current');
            }
            if (atributeName.endsWith('weaponType')){
                type = attributes[i].get('current');
            }
            if (atributeName.endsWith('weaponTarget')){
                target = attributes[i].get('current');
            }
            if (atributeName.endsWith('weaponAttack')){
                attack = attributes[i].get('current');
            }
            if (atributeName.endsWith('weaponDmg')){
                damage = attributes[i].get('current');
            }
            if (atributeName.endsWith('weaponQuantity')){
                ammo = attributes[i].get('current');
            }
            if (atributeName.endsWith('weaponRange')){
                range = attributes[i].get('current');
            }
            if (atributeName.endsWith('weaponSpeed')){
                speed = attributes[i].get('current');
            }
            if (atributeName.endsWith('weaponAP')){
                ap = attributes[i].get('current');
            }

        }
    }
    if (found) {
        return {id:id, name:name, pylon:pylon, type:type, attack:attack, target:target, damage:damage, quantity:ammo, range:range, speed:speed, ap:ap};
    }
    return;
}

function tokenDistance(token1, token2) {
    if (token1.get('pageid') != token2.get('pageid')) {
        log('Cannot measure distance between tokens on different pages');
        return;
    }

    var distance;

    var page = getObj('page', token1.get('pageid'));
    var gridType = page.get('grid_type');

    switch(gridType) {
        case 'hex':
        distance = hexVDistance([token1.get("left"), token1.get("top")], [token2.get("left"), token2.get("top")]);
        break;
        case 'hexr':
        distance = hexHDistance([token1.get("left"), token1.get("top")], [token2.get("left"), token2.get("top")]);
        break;
    }

    return distance;
}

function hexHDistance(unit1, unit2) {
    var q1, q2, r1, r2;
    q1 = Math.round((unit1[0] - 46.48512749037782) / 69.58512749037783);
    r1 = Math.round((unit1[1] - 39.8443949917523) / 39.8443949917523);
    r1 = Math.floor(r1 / 2);
    q2 = Math.round((unit2[0] - 46.48512749037782) / 69.58512749037783);
    r2 = Math.round((unit2[1] - 39.8443949917523) / 39.8443949917523);
    r2 = Math.floor(r2 / 2);

    return cubeDistance(oddQToCube(q1, r1), oddQToCube(q2, r2));
}

function hexVDistance(unit1, unit2) {
    var q1, q2, r1, r2;
    q1 = Math.round((unit1[0] - 37.59928099223013) / 37.59928099223013);
    r1 = Math.round((unit1[1] - 43.86582782426834) / 66.96582782426833);
    q1 = Math.floor(q1 / 2);
    q2 = Math.round((unit2[0] - 37.59928099223013) / 37.59928099223013);
    r2 = Math.round((unit2[1] - 43.86582782426834) / 66.96582782426833);
    q2 = Math.floor(q2 / 2);

    return cubeDistance(oddRToCube(q1, r1), oddRToCube(q2, r2));
}

function oddRToCube(q, r) {
    var x, y, z;
    x = q - (r - (r & 1)) / 2;
    z = r;
    y = -x - z;

    return [x, y, z];
}

function oddQToCube(q, r) {
    var x, y, z;
    x = q;
    z = r - (q - (q & 1)) / 2;
    y = -x - z;

    return [x, y, z];
}

function cubeDistance(cube1, cube2) {

    return Math.max(Math.abs(cube1[0] - cube2[0]), Math.abs(cube1[1] - cube2[1]), Math.abs(cube1[2] - cube2[2]));
}